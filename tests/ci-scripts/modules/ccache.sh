
citbx_module_ccache_define() {
    bashopts_declare -n CCACHE_DIR -l ccache-dir -t string -d "CCACHE directory" -s -v "$HOME/.ccache"
    bashopts_declare -n CITBX_CCACHE_DISABLE -l ccache-disable -t boolean -d "Disable CCACHE"
}

citbx_module_ccache_setup() {
    if [ "$CITBX_CCACHE_DISABLE" == "true" ]; then
        citbx_docker_run_add_args -e "CCACHE_DISABLE=1"
    else
        mkdir -p $CCACHE_DIR
        citbx_docker_run_add_args -e "CCACHE_DIR=$CCACHE_DIR" -v "$CCACHE_DIR:$CCACHE_DIR"
    fi
}

citbx_module_ccache_before() {
    if [ "$CITBX_CCACHE_DISABLE" == "true" ]; then
        CCACHE_DISABLE=1
    elif [ -d "$CCACHE_DIR" ]; then
        export PATH="/usr/lib/ccache:$PATH"
        export USE_CCACHE=1
        export NDK_CCACHE=${NDK_CCACHE:-$(which ccache)}
        print_info "CCACHE enabled"
        ccache -s
    fi
}

citbx_module_ccache_after() {
    if [ "$CITBX_CCACHE_DISABLE" != "true" ] \
        && [ -d "$CCACHE_DIR" ]; then
        ccache -s
    fi
}
